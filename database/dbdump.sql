-- MySQL dump 10.13  Distrib 5.6.28, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: myevents
-- ------------------------------------------------------
-- Server version	5.6.28-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'Lublin'),(2,'Warsaw'),(3,'Lvov'),(4,'Not Specified');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_participant`
--

DROP TABLE IF EXISTS `event_participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_participant` (
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_participant`
--

LOCK TABLES `event_participant` WRITE;
/*!40000 ALTER TABLE `event_participant` DISABLE KEYS */;
INSERT INTO `event_participant` VALUES (1,1),(11,1),(9,1),(2,1),(1,7),(1,8),(2,9),(12,9),(12,10),(13,10),(14,10);
/*!40000 ALTER TABLE `event_participant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_at` datetime NOT NULL,
  `end_at` datetime NOT NULL,
  `sport_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'Absolutely new event','Nadbys. 31','2016-02-14 01:39:44','0000-00-00 00:00:00',1,1,'2016-02-12 19:00:37','2016-02-13 21:47:25','I want\r\nto add\r\ndescription',2),(2,'Absolutely new event','','2016-02-14 01:39:44','0000-00-00 00:00:00',2,1,'2016-02-12 19:00:59','2016-02-12 19:00:59','',1),(3,'Absolutely new event','','2016-02-14 01:39:44','0000-00-00 00:00:00',2,1,'2016-02-12 19:01:01','2016-02-12 19:01:01','',1),(4,'Absolutely new event','','2016-02-14 01:39:44','0000-00-00 00:00:00',2,1,'2016-02-12 19:01:26','2016-02-12 19:01:26','',1),(5,'Absolutely new event','','2016-02-14 01:39:44','0000-00-00 00:00:00',2,1,'2016-02-12 21:23:36','2016-02-12 21:23:36','',1),(6,'Absolutely new event','','2016-02-14 01:39:44','0000-00-00 00:00:00',2,1,'2016-02-12 21:24:20','2016-02-12 21:24:20','',1),(7,'Absolutely new event','','2016-02-14 01:39:44','0000-00-00 00:00:00',2,1,'2016-02-12 21:25:32','2016-02-12 21:25:32','',1),(8,'Absolutely new event','','2016-02-14 01:39:44','0000-00-00 00:00:00',2,1,'2016-02-12 21:25:34','2016-02-12 21:25:34','',1),(9,'Sport game','','2016-02-14 01:39:44','0000-00-00 00:00:00',1,1,'2016-02-12 21:25:35','2016-02-13 21:28:14','',1),(10,'Absolutely new event','','2016-02-14 01:39:44','0000-00-00 00:00:00',2,1,'2016-02-12 21:25:36','2016-02-12 21:25:36','',1),(11,'I am an old event','Some address','2016-02-14 01:39:44','0000-00-00 00:00:00',2,1,'2016-02-13 17:08:06','2016-02-13 17:15:23','Bla\r\nSecond\r\nThird third\r\nFourth',1),(12,'Cave Johnson event','sfasdfasdf','0000-00-00 00:00:00','0000-00-00 00:00:00',3,9,'2016-02-14 10:09:17','2016-02-14 10:09:17','I want to make\r\na golf match',2),(13,'Basketball match','asfdasdflkjasdf','2017-10-20 00:00:00','2017-10-20 00:00:00',1,10,'2016-02-14 11:11:34','2016-02-14 11:11:34','sadfasdf',1),(14,'I want to autosubscribe','asdfasdf','0000-00-00 00:00:00','0000-00-00 00:00:00',1,10,'2016-02-14 11:13:14','2016-02-14 11:13:14','',1);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_02_12_192331_create_events_table',2),('2016_02_12_223512_create_event_participant',3),('2016_02_13_144727_add_description_to_events',4),('2016_02_13_194246_create_sports_table',5),('2016_02_13_233202_create_cities_table',6),('2016_02_13_233733_add_city_to_event',7),('2016_02_13_233742_add_city_to_user',7),('2016_02_14_001602_add_fields_to_user',8),('2016_02_14_002814_modify_user_name',9),('2016_02_14_010542_create_teams_table',10),('2016_02_14_015921_add_city_to_team',11),('2016_02_14_030111_add_team_users_table',12),('2016_02_14_111438_add_sport_to_team',13);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sports`
--

DROP TABLE IF EXISTS `sports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sports`
--

LOCK TABLES `sports` WRITE;
/*!40000 ALTER TABLE `sports` DISABLE KEYS */;
INSERT INTO `sports` VALUES (1,'Football'),(2,'BasketBall'),(3,'Golf');
/*!40000 ALTER TABLE `sports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_user`
--

DROP TABLE IF EXISTS `team_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_user` (
  `team_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_user`
--

LOCK TABLES `team_user` WRITE;
/*!40000 ALTER TABLE `team_user` DISABLE KEYS */;
INSERT INTO `team_user` VALUES (1,7),(1,1),(2,1),(3,1),(2,9),(2,10);
/*!40000 ALTER TABLE `team_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `sport_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,'Eagles',7,'2016-02-14 00:42:26','2016-02-14 00:42:26',1,1),(2,'New lords',1,'2016-02-14 09:12:45','2016-02-14 09:24:12',2,1),(3,'Footballsers',1,'2016-02-14 09:18:23','2016-02-14 09:18:23',1,2);
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'borisano@gmail.com','$2y$10$Lo6mHOshs1/NPEFApwmey.0wzjG4JadCk6U6r5RLS7RLqEunDhbCG','UCfbhuQiQWtWr6vx3ICpy6UGeuUpfs0GF3vKynrSUe3Ump5wLvQGZVgoU9si','2016-02-12 17:44:17','2016-02-14 09:51:04',3,'Something new','123453','Boris','Anosdf'),(2,'Tara.Hoppe@gmail.com','1111',NULL,'2016-02-12 20:56:15','2016-02-12 20:56:15',2,'','','',''),(3,'Richie.Mayer@hotmail.com','1111',NULL,'2016-02-12 20:56:30','2016-02-12 20:56:30',2,'','','',''),(4,'Irving98@Mayer.com','1111',NULL,'2016-02-12 20:56:31','2016-02-12 20:56:31',2,'','','',''),(5,'Edward.Lebsack@Steuber.biz','1111',NULL,'2016-02-12 20:56:33','2016-02-12 20:56:33',2,'','','',''),(6,'Willms.Rebecca@gmail.com','1111',NULL,'2016-02-12 20:56:34','2016-02-12 20:56:34',1,'','','',''),(7,'kanecrane@gmail.com','$2y$10$ATb4YO7AoEzyZD9KLVhO4.F.uxvQVSdgl3WdLqb6XZ35SrI7B75La',NULL,'2016-02-13 22:53:14','2016-02-13 22:53:14',0,'','','Kane','Crane'),(8,'ihordafter@gmail.com','$2y$10$mTrkev.Ui/3Fde6NUS9GTerosPw1I8uvaot8tovaOAjl4/J1RThvC','I61CY6s73Guki5xmIfdRiyIwdwn8f1sSdWOvGJlZW4aIAKfbBH4vgp0GyVE3','2016-02-14 09:51:36','2016-02-14 10:06:55',0,'','','Ihor','Dafter'),(9,'cg@gmail.com','$2y$10$O3679cmsjeY0W5IhmFIBee/r7ow8w1tQM7vRVm8eu8keWgCZZaTly','GCdHmbth53ubROlW2MTQEnG7WDRLOAClSp5pMXNo3pDDg1WUDARKXCNB0VcM','2016-02-14 10:08:07','2016-02-14 11:03:30',1,'F.Kona 54/34','123456','Cave','Johnson'),(10,'afraid.of.spiders828@gmail.com','$2y$10$NOzHbEvZ0SICfin437pFQO5K.RgAMdFCLDEz23TdK/ZZpfpxEf/2K',NULL,'2016-02-14 11:08:33','2016-02-14 11:09:23',3,'safd','1231231134124','Wlad','Yarotski');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-16 20:09:13
