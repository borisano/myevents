<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sports extends Model
{
    public function events() {
      return $this->hasMany('App\Events');
    }

    public function teams() {
      return $this->hasMany('App\Team');
    }

    public static function all_for_select($include_empty=false) {
      $sports = self::all();

      $sports_array = [];
      foreach ($sports as $sport) {
        $sports_array[$sport->id] = $sport->title;
      }

      if( $include_empty ) {
        return array_merge(['-1' => ''], $sports_array);
      } else {
        return $sports_array;
      }
    }
}
