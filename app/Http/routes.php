<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::group(['as'=> 'user::'],function(){
      Route::get('/', 'UserController@home')->name('home');
      Route::get('/user/edit', 'UserController@edit')->name('edit');
      Route::get('/user/{id}', 'UserController@show')->name('show');
      Route::put('/user', 'UserController@store')->name('store');
    });

    Route::group(['as'=>'team::'],function(){
      Route::get('/teams', 'TeamController@index')->name('index');
      Route::get('/teams/create', 'TeamController@create')->name('create');
      Route::get('/teams/{id}', 'TeamController@show')->name('show');

      Route::get('/teams/{id}/edit', 'TeamController@edit')->name('edit');

#TODO CSRF here
      Route::post('/teams', 'TeamController@store')->name('store');
      Route::put('/teams', 'TeamController@store')->name('update');

      Route::get('/teams/{id}/signin', 'TeamController@signin')->name('signin');
      Route::get('/teams/{id}/signout', 'TeamController@signout')->name('signout');
    });

    Route::group(['as' => 'event::'], function(){
      Route::get('/events', 'EventController@index')->name('index');

      Route::get('/events/create', 'EventController@create')->name('create');
      Route::get('/events/{id}', 'EventController@show')->name('show');

      Route::get('/events/{id}/edit', 'EventController@edit')->name('edit');

#TODO CSRF here
      Route::post('/events', 'EventController@store')->name('store');
      Route::put('/events', 'EventController@store')->name('update');

      #TODO csrf protection here!
      Route::get('/events/{id}/signin', 'EventController@signin')->name('signin');
      #TODO csrf protection here!
      Route::get('/events/{id}/signout', 'EventController@signout')->name('signout');


      Route::delete('/events/{event}', 'EventController@destroy')->name('destroy');
    });
});
