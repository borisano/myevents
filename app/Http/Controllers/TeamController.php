<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
      $teams_query = \App\Team::with('creator', 'members');
      if($request->sport_id) {
        $teams_query->where('sport_id','=',$request->sport_id);
      }
      if($request->city_id) {
        $teams_query->where('city_id','=', $request->city_id);
      }
      $teams = $teams_query->get();

      $search_params = [
        'url' => route('team::index'),
        'sport_id' => $request->sport_id,
        'city_id'  => $request->city_id
      ];

      return view('teams.index',[
        'teams' => $teams,
        'search_params' => $search_params
      ]);
    }

    public function show($id) {
      $team = \App\Team::findOrFail($id);

      return view('teams.show',[
        'team' => $team
      ]);
    }

    public function create() {
      $team = new \App\Team;
      return view('teams.create',[
        'team'=> $team
      ]);
    }

    public function edit($id, Request $request) {
      $team = $request->user()->own_teams()->findOrFail($id);

      return view('teams.edit',[
        'team'=> $team
      ]);
    }

    public function store(Request $request) {
      $this->validate($request, [
        'name' => 'required|max:255',
        'city_id'=> 'required',
        'sport_id'=> 'required'
      ]);

      $team = $request->user()->own_teams()->updateOrCreate(
          ['id'=> $request->id],
          [
            'name' => $request->name,
            'city_id'=> $request->city_id,
            'sport_id'=> $request->sport_id
          ]
        );

      \Flash::message('Team was saved successfully');

      $show_route = route('team::show',['id'=> $team->id]);
      return \Redirect::to( $show_route );
    }

    public function signin($id, Request $request) {
      $team = \App\Team::findOrFail($id);
      $current_user = $request->user();

      if( !$current_user->is_member($team)) {
        $team->members()->attach($current_user);

        \Flash::message('You are now part of the team');
      } else {
        \Flash::error('You are already part of the team');
      }

      $show_route = route('team::show',['id'=> $team->id]);
      return \Redirect::to( $show_route );
    }

    public function signout($id, Request $request) {
      $team = \App\Team::findOrFail($id);
      $current_user = $request->user();

      $team->members()->detach($current_user);

      \Flash::message('You left the team');

      $show_route = route('team::show',['id'=> $team->id]);
      return \Redirect::to( $show_route );
    }
}
