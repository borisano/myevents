<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth', ['except'=>['index','show']]);
    }

    public function index(Request $request) {
      $current_user = \Auth::user();

      $events_query = \App\Event::with('owner', 'participants');
      if($request->sport_id) {
        $events_query->where('sport_id','=',$request->sport_id);
      }
      if($request->city_id) {
        $events_query->where('city_id','=', $request->city_id);
      }
      $events = $events_query->get();


      $search_params = [
        'url' => route('event::index'),
        'sport_id' => $request->sport_id,
        'city_id'  => $request->city_id
      ];

      return view('events.index',[
        'events'        => $events,
        'search_params' => $search_params
      ]);
    }

    public function create() {
      $event = new \App\Event;
      return view('events.create',[
        'event'=> $event
      ]);
    }

    public function edit($id, Request $request) {
      $event = $request->user()->own_events()->findOrFail($id);

      return view('events.edit',[
        'event'=> $event
      ]);
    }

    public function store(Request $request) {
      $this->validate($request, [
          'title' => 'required|max:255',
      ]);

      $new_event = $request->user()->own_events()->updateOrCreate(
        ['id'=> $request->id],
        [
          'title'       => $request->title,
          'address'     => $request->address,
          'start_at'    => $request->start_at,
          'end_at'      => $request->end_at,
          'description' => $request->description,
          'sport_id'    => $request->sport_id,
          'city_id'     => $request->city_id,
      ]);


      \Flash::message('Event was saved');

      $show_route = route('event::signin',['id'=> $new_event->id]);
      return \Redirect::to( $show_route );
    }

    public function show($id) {
      $event = \App\Event::findOrFail($id);

      return view('events.show', [
        'event' => $event
      ]);
    }

    public function signin($id, Request $request) {
      $event = \App\Event::findOrFail($id);
      $current_user = $request->user();

      if( !$current_user->is_participating($event)) {
        $event->participants()->attach($current_user);

        \Flash::message('You will participate in event');
      } else {
        \Flash::error('You are already a participant of the event');
      }

      $show_route = route('event::show',['id'=> $event->id]);
      return \Redirect::to( $show_route );
    }

    public function signout($id, Request $request) {
      $event = \App\Event::findOrFail($id);
      $current_user = $request->user();

      $event->participants()->detach($current_user);

      \Flash::message('You will not participate in event');

      $show_route = route('event::show',['id'=> $event->id]);
      return \Redirect::to( $show_route );
    }
}
