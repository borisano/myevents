<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function home() {
      $current_user = \Auth::user();

      $show_route = route('user::show',['id'=> $current_user->id]);
      return \Redirect::to( $show_route );
    }

    public function show($id) {
      $user = \App\User::findOrFail($id);

      return view('users.show', [
        'user' => $user
      ]);
    }

    public function edit() {
      $user = \Auth::user();

      return view('users.edit',[
        'user' => $user
      ]);
    }

    public function store(Request $r) {
      $this->validate($r, [
          'first_name' => 'required|max:255',
          'last_name' => 'required|max:255',
      ]);

      $user = \Auth::user();
      $user->update([
        'first_name'=> $r->first_name,
        'last_name' => $r->last_name,
        'city_id'   => $r->city_id,
        'address'   => $r->address,
        'phone'     => $r->phone
      ]);

      \Flash::message('Information has been updated');

      return \Redirect::to( route('user::home'));
    }
}
