<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
      'title',
      'address',
      'start_at',
      'end_at',
      'description',
      'sport_id',
      'city_id'
    ];


    public function sport() {
      return $this->belongsTo('App\Sports');
    }

    public function sport_title() {
      if($this->sport) {
          return $this->sport->title;
      }
      return '';
    }

    public function city() {
      return $this->belongsTo('App\City');
    }

    public function city_name() {
      if($this->city) {
          return $this->city->name;
      }
      return '';
    }

    public function owner() {
        return $this->belongsTo('App\User', 'owner_id');
    }

    public function participants() {
      return $this->belongsToMany('App\User', 'event_participant');
    }
}
