<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{

    protected $fillable = [
      'name', 'creator_id', 'city_id'
    ];

    public function creator() {
      return $this->belongsTo('App\User', 'creator_id');
    }

    public function members() {
      return $this->belongsToMany('App\User');
    }

    public function city() {
      return $this->belongsTo('App\City');
    }

    public function city_name() {
      if($this->city) {
        return $this->city->name;
      }
      return '';
    }

    public function sport() {
      return $this->belongsTo('App\Sports');
    }

    public function sport_name() {
      if($this->sport) {
        return $this->sport->title;
      }
      return '';
    }
}
