<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function events() {
      return $this->hasMany('App\Events');
    }

    public function users() {
      return $this->hasMany('App\User');
    }

    public function teams() {
      return $this->hasMany('App\Team');
    }

    public static function all_for_select($include_empty=false) {
      $cities = self::all();

      $cities_array = [];
      foreach ($cities as $city) {
        $cities_array[$city->id] = $city->name;
      }

      if( $include_empty ) {
        return array_merge(['-1' => ''], $cities_array);
      } else {
        return $cities_array;
      }

    }
}
