<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name',
        'email', 'password',
        'city_id', 'address',
        'phone'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function full_name() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function city() {
      return $this->belongsTo('App\City');
    }

    public function city_name() {
        if($this->city) {
            return $this->city->name;
        }
        return '';
    }

    public function own_teams() {
      return $this->hasMany('App\Team', 'creator_id');
    }

    public function is_creator($team) {
        return $this->own_teams->contains($team);
    }


    public function teams() {
        return $this->belongsToMany('App\Team');
    }

    public function is_member($team) {
        return $this->teams->contains($team);
    }

    public function own_events() {
        return $this->hasMany('App\Event', 'owner_id');
    }

    public function participating_events() {
        return $this->belongsToMany('App\Event', 'event_participant');
    }

    // Check if user is participating in given event
    public function is_participating($event) {
        return $this->participating_events->contains($event);
    }

    //Check if user is the author of the given event
    public function is_author($event) {
        return $this->own_events->contains($event);
    }
}
