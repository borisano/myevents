{!! Form::open(['url' => $params['url'], 'method'=>'GET', 'class'=> 'form-inline pull-right']) !!}
  <div class="form-group">
    {!! Form::label('sport_id', 'Category');!!}
    {!! Form::select(
          'sport_id',
          App\Sports::all_for_select(true),
          $params['sport_id'] ? $params['sport_id'] : '-1',
          ['class'=> 'form-control']);
    !!}
  </div>
  <div class="form-group">
    {!! Form::label('city_id', 'City');!!}
    {!! Form::select(
          'city_id',
          App\City::all_for_select(true),
          $params['city_id'] ? $params['city_id'] : '-1',
          ['class'=> 'form-control']);
    !!}
  </div>
  <div class="form-group">
    {!! Form::submit( 'Filter', ['class'=> 'form-control btn btn-primary'] ); !!}
  </div>
  <div class="form-group">
    <a href="{{$params['url']}}" class="btn btn-warning">Reset Filters</a>
  </div>
{!! Form::close() !!}