@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    Current user email is:
                    {{ $current_user->email }}
                </div>
            </div>
            <ul>
            @foreach ($current_user->own_events as $event)
                <li>{{ $event->title }}</li>
            @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection
