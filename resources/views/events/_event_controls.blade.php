  @if (isset($user) && $user->is_participating($event))
    <a href="{{route('event::signout', ['id'=>$event->id])}}" class="btn btn-danger" >I won't go there</a>
  @elseif(isset($user))
    <a href="{{route('event::signin', ['id'=>$event->id])}}" class="btn btn-primary">I will go there </a>
  @endif

  @if (isset($user) && $user->is_author($event))
    <a href="{{route('event::edit', ['id'=>$event->id])}}" class="btn btn-warning">Edit this event</a>
  @endif