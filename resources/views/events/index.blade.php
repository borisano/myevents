@extends('layouts.app')

@section('title')
Events List
@stop

@section('content')
  <div class="panel">
    <div class="panel-heading">
      <a href="{{route('event::create')}}" class="btn btn-success">
        Create new Event
      </a>
      @include('common/_search_form', [
        'params' => $search_params
      ])
    </div>
    <div class="panel-body">
      <table class="table">
        <tr>
          <th>ID</th>
          <th>Title</th>
          <th>Sport</th>
          <th>Author</th>
          <th>City</th>
          <th>Participants</th>
          <th>Actions</th>
        </tr>
        @forelse ($events as $event)
          <tr>
            <td>
              <a href="{{route('event::show', ['id' => $event->id])}}">{{ $event->id }}</a>
            </td>
            <td>{{ $event->title }}</td>
            <td>{{ $event->sport_title()}}</td>
            <td>
              <a href="{{route('user::show',['id'=>$event->owner->id])}}">
                {{ $event->owner->full_name() }}
              </a>
            </td>
            <td>{{ $event->city_name() }}</td>
            <td>
                @if ( count($event->participants) )
                  {{ count($event->participants)}} people will go.
                @else
                  No participants yet. Be the first one!
                @endforelse
            </td>
            <td>
              <a href="{{route('event::show', ['id' => $event->id])}}" class="btn btn-default">View Event</a>
              @include('events._event_controls', [
                'user' => Auth::user(),
                'event'=> $event
              ])
            </td>
          </tr>
        @empty
          <tr>No events to display</tr>
        @endforelse
      </table>
      <div class="pan">
        Totally {{ $events->count() }} rows fetched.
      </div>
    </div>
  </div>
@stop