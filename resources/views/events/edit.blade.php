@extends('layouts.app')

@section('title')
Edit event: {{$event->title}}
@stop

@section('content')

@include('common.errors')

<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <h1>Edit the event</h1>
    @include('events/_form', [
      'event'=> $event,
      'url' => route('event::store'),
      'method' => 'PUT'
    ])
  </div>
  <div class="col-md-3"></div>
</div>
@stop