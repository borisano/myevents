@extends('layouts.app')

@section('title')
Create New Event
@stop

@section('content')

@include('common.errors')

<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <h1>Create New Event</h1>
    @include('events/_form', [
      'event'=> $event,
      'url' => route('event::store'),
      'method' => 'POST'
    ])
  </div>
  <div class="col-md-3"></div>
</div>
@stop