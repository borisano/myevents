{!! Form::open(['url' => $url, 'method'=>$method]) !!}
    {!! Form::hidden('id', $event->id); !!}
  <div class="form-group">
    {!! Form::label('title', 'Event Title');!!}
    {!! Form::text('title', $event->title, ['class'=> 'form-control']); !!}
  </div>
  <div class="form-group">
    {!! Form::label('city_id', 'City');!!}
    {!! Form::select('city_id', App\City::all_for_select(), $event->city_id, ['class'=> 'form-control']); !!}
  </div>
  <div class="form-group">
    {!! Form::label('address', 'Event Address');!!}
    {!! Form::text('address', $event->address, ['class'=> 'form-control']); !!}
  </div>
  <div class="form-group">
    {!! Form::label('start_at', 'Event Start Time');!!}
    {!! Form::text('start_at', $event->start_at, ['class'=> 'form-control']); !!}
  </div>
  <div class="form-group">
    {!! Form::label('end_at', 'Event End Time');!!}
    {!! Form::text('end_at', $event->end_at, ['class'=> 'form-control']); !!}
  </div>
  <div class="form-group">
    {!! Form::label('description', 'Description');!!}
    {!! Form::textArea('description', $event->description, ['class'=> 'form-control', 'rows' => 4]); !!}
  </div>
  <div class="form-group">
    {!! Form::label('sport_id', 'Category');!!}
    {!! Form::select('sport_id', App\Sports::all_for_select(), $event->sport_id, ['class'=> 'form-control']); !!}
  </div>
  <div class="form-group">
    {!! Form::submit( $event->id ? 'Save Changes' : 'Create Event', ['class'=> 'form-control btn btn-primary'] ); !!}
  </div>
{!! Form::close() !!}