@extends('layouts.app')

@section('title')
View Event: {{$event->title}}
@stop

@section('content')


  @include('events._event_controls', [
    'user' => Auth::user(),
    'event'=> $event
  ])

  <div class="row">
    <div class="col-md-5">
      <div class="panel panel-default">
        <div class="panel-heading">{{$event->title}}</div>
        <div class="panel-body">
          {!! nl2br($event->description) !!}
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">Event Details</div>
        <div class="panel-body">
          <div>
            <h6><em>Event category:</em></h6>
            {{ $event->sport->title }}
          </div>
          <div>
            <h6><em>Event Address:</em></h6>
            {{$event->city_name()}}, {{$event->address}}
          </div>
          <div>
            <h6><em>Event starts at:</em></h6>
            {{ date('F j, Y, g:i a', strtotime($event->start_at)) }}
          </div>
          <div>
            <h6><em>Event ends at:</em></h6>
              {{ date('F j, Y, g:i a', strtotime($event->end_at)) }}
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-2">
      <div class="panel panel-default">
        <div class="panel-heading">{{count($event->participants)}} participants</div>
        <div class="panel-body">
          <ul>
            @foreach ( $event->participants as $p)
              <li>
                <a href="{{route('user::show',['id'=>$p->id])}}">{{$p->full_name()}}</a>

              </li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>

  @include('events._event_controls', [
    'user' => Auth::user(),
    'event'=> $event
  ])
@stop