@extends('layouts.app')

@section('title')
Teams
@stop

@section('content')
  <div class="panel">
    <div class="panel-heading">
      <a href="{{route('team::create')}}" class="btn btn-success">
        Create new Team
      </a>
      @include('common/_search_form', [
        'params' => $search_params
      ])
    </div>
    <div class="panel-body">
      <table class="table">
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Creator</th>
          <th>Sport</th>
          <th>City</th>
          <th>Participants</th>
          <th>Actions</th>
        </tr>
        @forelse ($teams as $team)
          <tr>
            <td>
              <a href="{{route('team::show', ['id' => $team->id])}}">{{ $team->id }}</a>
            </td>
            <td>{{ $team->name }}</td>
            <td>
              <a href="{{route('user::show',['id'=>$team->creator->id])}}">
                {{ $team->creator->full_name() }}
              </a>
            </td>
            <td>{{ $team->sport_name() }}</td>
            <td>{{ $team->city_name() }}</td>
            <td>
              @if( count($team->members) )
                There are {{ count($team->members) }} members.
              @else
                There are no members yet
              @endif
            </td>
            <td>
              <a href="{{route('team::show', ['id' => $team->id])}}" class="btn btn-default">View Team Details</a>
              @include('teams._team_controls', [
                'user' => Auth::user(),
                'team'=> $team
              ])
            </td>
          </tr>
        @empty
          <tr>No teams to display</tr>
        @endforelse
      </table>
      <div class="pan">
        Totally {{ $teams->count() }} rows fetched.
      </div>
    </div>
  </div>
@stop