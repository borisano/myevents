@extends('layouts.app')

@section('title')
View Team: {{$team->name}}
@stop

@section('content')


  @include('teams._team_controls', [
    'user' => Auth::user(),
    'team'=> $team
  ])

    <div class="col-md-2">
      <div class="panel panel-default">
        <div class="panel-heading">Team {{$team->name}} details</div>
        <div class="panel-body">
          <div>
            <h6><em>City:</em></h6>
            {{ $team->city->name }}
          </div>
          <div>
            <h6><em>Creator:</em></h6>
            <a href="{{route('user::show',['id'=>$team->creator->id])}}">{{ $team->creator->full_name() }}</a>

          </div>
          <div>
            <h6><em>Sport:</em></h6>
            {{ $team->sport_name() }}
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading">{{count($team->members)}} participants</div>
        <div class="panel-body">
          <ul>
            @foreach ( $team->members as $member)
              <li>
                <a href="{{route('user::show',['id'=>$member->id])}}">{{$member->full_name()}}</a>
              </li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
@stop