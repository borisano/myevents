{!! Form::open(['url' => $url, 'method'=>$method]) !!}
  {!! Form::hidden('id', $team->id); !!}
  <div class="form-group">
    {!! Form::label('name', 'Name');!!}
    {!! Form::text('name', $team->name, ['class'=> 'form-control']); !!}
  </div>
  <div class="form-group">
    {!! Form::label('city_id', 'City');!!}
    {!! Form::select('city_id', App\City::all_for_select(), $team->city_id, ['class'=> 'form-control']); !!}
  </div>
  <div class="form-group">
    {!! Form::label('sport_id', 'Sport');!!}
    {!! Form::select('sport_id', App\Sports::all_for_select(), $team->sport_id, ['class'=> 'form-control']); !!}
  </div>
  <div class="form-group">
    {!! Form::submit( 'Save Changes', ['class'=> 'form-control btn btn-primary'] ); !!}
  </div>
{!! Form::close() !!}