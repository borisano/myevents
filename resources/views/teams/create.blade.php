@extends('layouts.app')

@section('title')
Create New Team
@stop

@section('content')

@include('common.errors')

<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <h1>Create New Team</h1>
    @include('teams/_form', [
      'team'=> $team,
      'url' => route('team::store'),
      'method' => 'POST'
    ])
  </div>
  <div class="col-md-3"></div>
</div>
@stop