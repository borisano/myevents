  @if ($user->is_member($team))
    <a href="{{route('team::signout', ['id'=>$team->id])}}" class="btn btn-danger" >Leave Team</a>
  @else
    <a href="{{route('team::signin', ['id'=>$team->id])}}" class="btn btn-primary">Join Team </a>
  @endif

  @if ($user->is_creator($team))
    <a href="{{route('team::edit', ['id'=>$team->id])}}" class="btn btn-warning">Edit Team</a>
  @endif