@extends('layouts.app')

@section('title')
Edit Team: {{$team->name}}
@stop

@section('content')

@include('common.errors')

<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <h1>Edit the team</h1>
    @include('teams/_form', [
      'team'=> $team,
      'url' => route('team::store'),
      'method' => 'PUT'
    ])
  </div>
  <div class="col-md-3"></div>
</div>
@stop