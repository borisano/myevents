@extends('layouts.app')

@section('title')
View User: {{$user->full_name()}}
@stop

@section('content')

<div class="row">
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">
      <span>User info</span>
      @if($user == \Auth::user())
        <div class="pull-right">
          <a href="{{route('user::edit')}}" class="btn btn-success">Edit Info</a>
        </div>
      @endif
      </div>
      <div class="panel-body">
        <dl>
          <dt>Name</dt>
          <dd>{{$user->full_name()}}</dd>
          <dt>Email</dt>
          <dd>{{$user->email}}</dd>
          <dt>Phone</dt>
          <dd>{{$user->phone}}</dd>
          <dt>City</dt>
          <dd>{{$user->city_name()}}</dd>
          <dt>Address</dt>
          <dd>{{$user->address}}</dd>
        </dl>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">{{ \Auth::user()->id == $user->id ? 'You ' : ($user->full_name())}} will go to</div>
      <div class="panel-body">
        <ul>
          @forelse ( $user->participating_events as $e)
            <li>
              <a href="{{route('event::show',['id'=>$e->id])}}">
                {{$e->title}}
              </a>
              at {{ date('F j, Y, g:i a', strtotime($e->start_at)) }}
            </li>
          @empty
            {{ \Auth::user()->id == $user->id ? 'You have ' : ($user->full_name() . ' has')}} no upcoming events
          @endforelse
        </ul>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="panel-default panel">
      <div class="panel-heading">Team created</div>
      <div class="panel-body">
          @forelse ( $user->own_teams as $team)
            <li>
              <a href="{{route('team::show',['id'=>$team->id])}}">
                {{$team->name}}
              </a>
            </li>
          @empty
            {{ \Auth::user() == $user ? 'You have ' : ($user->full_name() . ' has')}} created no teams.
          @endforelse
      </div>
    </div>
    <div class="panel-default panel">
      <div class="panel-heading">Member of Teams</div>
      <div class="panel-body">
        @forelse ( $user->teams as $team)
          <li>
            <a href="{{route('team::show',['id'=>$team->id])}}">
              {{$team->name}}
            </a>
          </li>
        @empty
          {{ \Auth::user()->id == $user->id ? 'You have ' : ($user->full_name() . ' has')}} subscribed to no teams.
        @endforelse
      </div>
    </div>
  </div>
</div>
@stop