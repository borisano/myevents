@extends('layouts.app')

@section('title')
Edit Profile
@stop

@section('content')

@include('common.errors')

<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <h1>Edit your profile</h1>
    @include('users/_form', [
      'user'=> $user,
      'url' => route('user::store'),
      'method'=> 'PUT'
    ])
  </div>
  <div class="col-md-3"></div>
</div>
@stop