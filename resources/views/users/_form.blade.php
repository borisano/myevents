{!! Form::open(['url' => $url, 'method'=>$method]) !!}
    {!! Form::hidden('id', $user->id); !!}
  <div class="form-group">
    <dl>
      <dt>Email</dt>
      <dd>{{$user->email}}</dd>
    </dl>
  </div>
  <div class="form-group">
    {!! Form::label('first_name', 'First Name');!!}
    {!! Form::text('first_name', $user->first_name, ['class'=> 'form-control']); !!}
  </div>
  <div class="form-group">
    {!! Form::label('last_name', 'Last Name');!!}
    {!! Form::text('last_name', $user->last_name, ['class'=> 'form-control']); !!}
  </div>
  <div class="form-group">
    {!! Form::label('phone', 'Phone');!!}
    {!! Form::text('phone', $user->phone, ['class'=> 'form-control']); !!}
  </div>

  <div class="form-group">
    {!! Form::label('city_id', 'City');!!}
    {!! Form::select('city_id', App\City::all_for_select(), $user->city_id, ['class'=> 'form-control']); !!}
  </div>
  <div class="form-group">
    {!! Form::label('address', 'Address');!!}
    {!! Form::text('address', $user->address, ['class'=> 'form-control']); !!}
  </div>

  <div class="form-group">
    {!! Form::submit( 'Save Changes', ['class'=> 'form-control btn btn-primary'] ); !!}
  </div>
{!! Form::close() !!}